namespace :user do
 desc 'create user with admin privilegies'
  task :create, [:login,:password] => :environment do |_, args|
    args.with_defaults(login:'admin@lvh.me', password: '123456')

    User.create!(
      email: args[:login],
      password: args[:password],
      is_admin: true,
      enabled:true
    )
 end
end
