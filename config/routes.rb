Rails.application.routes.draw do
  devise_for :users, controllers: {
    invitations: "invitations"
  }

  root to: "welcome#index"

  get '/:id', to: 'welcome#index'
  post '/', to: 'welcome#show', as: 'find_order_by_token'

  namespace :orders do
    resources :manage, controller: :manage
    put "change_status/:id", to: "manage#change_status", as: 'change_status'
  end

  namespace :users do
    resources :manage, controller: :manage
    put "change_status/:id", to: "manage#change_status", as: 'change_status'
  end

  namespace :items do
    resources :manage, controller: :manage
    put "change_status/:id", to: "manage#change_status", as: 'change_status'
  end
end
