class AddAttachmentImageToItem < ActiveRecord::Migration[5.0]
  def up
    change_table :items do |t|
      t.attachment :image
    end
  end

 def down
    remove_attachment :items, :image
 end
end
