class AddEnabledForItems < ActiveRecord::Migration[5.0]
  def change
    change_table :items do |t|
      t.boolean :enabled,  null: false, default: true
    end
  end
end
