class CreateOrdered < ActiveRecord::Migration[5.0]
  def change
    create_table :ordereds do |t|
      t.belongs_to :item, index: true
      t.belongs_to :order, index: true
      t.integer :count
      t.boolean :coming

      t.timestamps
    end
  end
end
