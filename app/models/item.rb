class Item < ApplicationRecord
  has_many :ordered
  has_many :order, through: :ordered
  validates :name, uniqueness: true, presence: true
  
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/missing.jpg"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
