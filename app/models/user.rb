class User < ApplicationRecord

  devise :invitable, :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :order       
  validates :email, presence: true, uniqueness: true
end
