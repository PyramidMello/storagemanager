class Order < ApplicationRecord

  has_many :ordered, inverse_of: :order, dependent: :destroy
  accepts_nested_attributes_for :ordered, allow_destroy: true
  has_many :item, through: :ordered, dependent: :destroy

  belongs_to :user

  validates :ordered, presence: true

  default_value_for :token do
    token ||= [*'A'..'Z'].sample(8).join
    token
  end

end
