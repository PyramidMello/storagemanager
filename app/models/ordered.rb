class Ordered < ApplicationRecord
    belongs_to :item
    belongs_to :order

    validates :count, presence: true, numericality: {only_integer: true, greater_than: 0}

    default_value_for :coming, false
end
