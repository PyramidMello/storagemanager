module Users
  class ManageController < ApplicationController
    before_action :authorize_admin!

    def index
      @users = scope
    end

    def destroy
      @user = scope.find(params[:id])
      @user.destroy
      redirect_to users_manage_index_path
    end

    def change_status
      @user = scope.find(params[:id])
      if !@user.invitation_accepted_at.nil?
        @user.enabled ? @user.enabled = false : @user.enabled = true
        @user.save
      end
      redirect_to users_manage_index_path
    end

    private

    def scope
      User.where(is_admin: false).order(id: :asc)
    end

  end
end
