class InvitationsController < Devise::InvitationsController
  before_filter :update_sanitized_params, only: :update


   protected

   def update_sanitized_params
     devise_parameter_sanitizer.permit(:accept_invitation, keys: [:first_name,:last_name, :password, :password_confirmation, :invitation_token])
   end
end
