module Items
  class ManageController < ApplicationController
    before_action :authorize_admin!

    def index
      @items = Item.all.order('id ASC')
    end

    def new
      @item = Item.new
    end

    def create
      @item = Item.new(item_params)
      @item.save
      redirect_to action: :index
    end

    def change_status
      @item = Item.find(params[:id])
      p @item
      @item.enabled ? @item.enabled = false : @item.enabled = true
      @item.save
      redirect_to items_manage_index_path
    end

    def destroy
      @item = Item.find(params[:id])
      @item.destroy
      redirect_to items_manage_index_path
    end

    private

    def item_params
      params.require(:item).permit(:name,:count,:image)
    end

  end
end
