class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  private

  def after_invite_path_for(resource)
    users_manage_index_path
  end

  def authenticate_inviter!
    authenticate_user!(:force => true)
    authorize_admin!
    enabled_user!
  end

  def authorize_admin!
    unless current_user.is_admin?
      redirect_to root_path
    end
  end

  def enabled_user!
    unless current_user.enabled?
      redirect_to root_path
    end
  end

end
